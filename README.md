# 综合管理平台

#### 项目介绍
综合管理平台Admin模板：前端Vue+中后台Flask

#### 软件架构
分为前后端两部分：
-前端 vue-cli 
-后端 python flask


#### 安装教程

1. Vue 安装：
    npm install
    npm run dev

2. flask 运行
    python run.py

3. vue集成到flask：
    -在vue中运行 npm run build:prod 生成dist文件夹,
    -复制/dist/目录下文件，到/templates, /static下
    -在flask中添加根目录路径链接
        @app.route('/')
        def index():
            return render_template('index.html')
    -flask 运行
        python run.py


#### VUE新建界面使用说明

文件目录结构：

-api：存放axios call请求路径

-store： vue状态管理器

-components： 各组件

-router： 树状目录结构以及访问路径

-views： 每页界面显示内容



vue文件：

views下存放的都是.vue文件，每个vue文件分为三部分template，script，style。

-template： html标签以及自定义标签，html静态网页部分

-script： js代码，vuejs，主要定义动态部分

-style：    表现界面样式，可省略



简单搭建一个界面：

1.在router/index.js下constantRouterMap变量中添加一项，所有属性可以参考其他。这是网页访问地址，同时也会自动生成左边树状目录内容。

2.在views中新创建一个界面，同时将该XXX.vue import到router中来，当点击树状目录时能跳转到该界面。

3.编辑该vue文件，绑定加载界面时需要用到的函数。



#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

