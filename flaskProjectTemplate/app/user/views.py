#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import request
import json
from app.user import user
from mockData import MockData

mockData = MockData()


@user.route("/login", methods=['POST'])
def login():
    if request.method == 'POST':
        reqParm = json.loads(request.data.decode())
        print('{user} login!!'.format(user=reqParm['username']))
    data = mockData.getRoles()[reqParm['username']]
    return str(data).replace('\'', '\"')


@user.route("/routes", methods=['get'])
def getRoutes():
    data = mockData.getRoutes()
    return str(data).replace('\'', '\"')


@user.route("/info")
def getInfo():
    userMap = mockData.getRoles()
    return str(userMap['admin']).replace('\'', '\"')


@user.route("/logout")
def logout():
    return 'success'
