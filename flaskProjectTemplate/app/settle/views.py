#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from flask import request
from app.settle import settle
from flask import make_response
from flask import send_from_directory


@settle.route("/upload", methods=['POST'])
def upload():
    print(request.files)
    file = request.files.get('file')
    if(file is not None):
        file.save(file.filename)

    return str('{"response":"sccuess"}').replace('\'', '\"')


@settle.route("/download", methods=['GET'])
def download_file():
    directory = os.getcwd()
    filename = "test.xlsx"
    response = make_response(send_from_directory(directory, filename, as_attachment=True))
    response.headers["Content-Disposition"] = "attachment; filename={}".format(filename.encode().decode('latin-1'))
    return response
