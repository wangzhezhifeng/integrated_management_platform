#!/usr/bin/python
# -*- coding: UTF-8 -*-
from flask import Flask
from flask_cors import CORS
from app.index import index
from app.user import user
from app.settle import settle

app = Flask(__name__)
CORS(app, supports_credentials=True)

app.register_blueprint(index, url_prefix='/')
app.register_blueprint(user, url_prefix='/user')
app.register_blueprint(settle, url_prefix='/settle')


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
