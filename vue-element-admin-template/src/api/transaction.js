import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/transaction/list',
    method: 'get',
    params: query
  })
}

export function uploadList(fileData) {
  return request({
    url: '/settle/upload',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data: fileData
  })
}

export function downLoadFile() {
  return request({
    url: '/settle/download',
    method: 'get',
    responseType: 'arrayBuffer'
  })
}


