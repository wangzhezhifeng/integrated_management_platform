文件目录结构：
-api：存放axios call请求路径
-store： vue状态管理器
-components： 各组件
-router： 树状目录结构以及访问路径
-views： 每页界面显示内容

vue文件：
views下存放的都是.vue文件，每个vue文件分为三部分template，script，style。
-template： html标签以及自定义标签，html静态网页部分
-script： js代码，vuejs，主要定义动态部分
-style：    表现界面样式，可省略

简单搭建一个界面：
1.在router/index.js下constantRouterMap变量中添加一项，所有属性可以参考其他。这是网页访问地址，同时也会自动生成左边树状目录内容。
2.在views中新创建一个界面，同时将该XXX.vue import到router中来，当点击树状目录时能跳转到该界面。
3.编辑该vue文件，绑定加载界面时需要用到的函数。
